package com.example.brand.article;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ArticleService {

    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public List<Article> getArticle() {
        return articleRepository.findAll();
    }

    public void findArticleById(Long articleId) {
        articleRepository.findAllById(Collections.singleton(articleId));
        boolean exists = articleRepository.existsById(articleId);
        if (!exists) {
            throw new IllegalStateException(
                    "article with id " + articleId + " doesn't exist");
        }
        articleRepository.findAllById(Collections.singleton(articleId));
    }

    public void addNewArticle(Article article) {
        Optional<Article> articleOptional = articleRepository
                .findArticleByName(article.getName());
        if (articleOptional.isPresent()) {
            throw new IllegalStateException("name taken");
        }
        articleRepository.save(article);
    }

    public void deleteArticle(Long articleId) {
        articleRepository.findById(articleId);
        boolean exists = articleRepository.existsById(articleId);
        if (!exists) {
            throw new IllegalStateException(
                    "article with id " + articleId + " doesn't exist");
        }
        articleRepository.deleteById(articleId);
    }

    @Transactional
    public void updateArticle(Long articleId,
                              String name,
                              String description) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new IllegalStateException("article with id " + articleId + " doesn't exist"));

        if (name != null && name.length() > 0 && !Objects.equals(article.getName(), name)) {
            article.setName(name);
        }

        if (description != null && description.length() > 0 && !Objects.equals(article.getDescription(), description)) {
            Optional<Article> articleOptional = articleRepository.findArticleByDescription(description);
            if (articleOptional.isPresent()) {
                throw new IllegalStateException("description taken");
            }
            article.setDescription(description);
        }
    }
}
