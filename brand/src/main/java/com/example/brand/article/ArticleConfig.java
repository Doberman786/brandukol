package com.example.brand.article;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.List;

@Configuration
public class ArticleConfig {

    @Bean
    CommandLineRunner commandLineRunner(
            ArticleRepository repository) {
        return args -> {
            Article memory = new Article(
                    "Branded Memory Stick",
                    "Branded 16 GB memory stick",
                    "USB flash drive",
                    17.89,
                    "NOK"
            );

            Article mug = new Article(
                    "Branded Drinking Mug",
                    "Porcelain drinking cup with your logo on it.",
                    "Mug",
                    0,
                    null
            );

            repository.saveAll(
                    List.of(memory, mug)
            );
        };
    }
}
