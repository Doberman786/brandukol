package com.example.brand.article;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

    @Query("SELECT a FROM Article a WHERE a.name = ?1")
    Optional<Article> findArticleByName(String name);

    @Query("SELECT a FROM Article a WHERE a.description = ?1")
    Optional<Article> findArticleByDescription(String description);

    //void findAllById(Long articleId);
}
