package com.example.brand.article;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path = "api/article")
public class ArticleController {
    private final ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping
    public List<Article> getArticle() {
        return articleService.getArticle();
    }

    @PostMapping
    public void registerNewArticle(@RequestBody Article article) {
        articleService.addNewArticle(article);
    }

    @GetMapping("api/article/{articleId}")
    public void findArticleById(@PathVariable("articleId") Long articleId) {
        articleService.findArticleById(articleId);
    }

    @DeleteMapping(path = "{articleId}")
    public void deleteArticle(@PathVariable("{articleId}") Long articleId) {
        articleService.deleteArticle(articleId);
    }

    @PutMapping(path = "{articleId}")
    public void updateArticle(
            @PathVariable("articleId") Long articleId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String description) {
        articleService.updateArticle(articleId, name, description);
    }
}
